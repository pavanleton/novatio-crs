import React, { ReactChild, useState } from "react";
import { Grid, Flex } from "@chakra-ui/react";
import { useRouter } from "next/router";

// COMPONENTS
import Breadcrumbs from "components/Breadcrumbs";
import SubMenuSideBar from "components/sidebars/SubMenu";

// SERVICES
import { getCategories } from "services/api/store";

const ServicesLayout: React.FC<{ children: ReactChild }> = ({ children }) => {
  const [categories, setCategories] = useState([]);

  const router = useRouter();
  const categoryId = router.query?.catId || 1;

  React.useEffect(() => {
    getCategories(categoryId as string | number).then(res => { console.log({res});
     setCategories(res?.data?.data?.attributes?.categories?.data || [])})
  }, [categoryId, router.pathname])

  return (
    <>
      <Grid w={"100%"} gridTemplateColumns={{ base: "1fr", lg: ".6fr 2fr" }}>
        <SubMenuSideBar list={categories} />
        <Flex py={"1rem"} flexDirection={"column"} height={'100vh'} overflowY={'scroll'} overflowX={'hidden'}  css={{
            '&::-webkit-scrollbar':{
              display: 'none'
            }
          }}>
          <Breadcrumbs />
          {children}
        </Flex>
      </Grid>
    </>
  );
};

export default ServicesLayout;
