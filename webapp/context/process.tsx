import React, { ReactChild, useState } from "react";
import { useRouter } from "next/router";

// SERVICES
import { getSubProcess, getParentProcess } from "services/api/store";

export type ProcessType = {
    id: number,
    title: string
}

export type SubProcessType = {
    id: number,
    title: string,
    pid: number, 
    isComplete: boolean,
    content: string
}

interface ProcessContextTypes {
    process: ProcessType[],
    subProcess: SubProcessType[],
    selectedProcess: number,
    updateSelection: (id: number) => void, 
    toggleSubProcess: (spId: number) => void, 
}

export const ProcessContext = React.createContext<ProcessContextTypes>({
    process: [],
    subProcess: [],
    selectedProcess: 1,
    updateSelection: (id: number) => {}, 
    toggleSubProcess: (spId: number) => {}, 
})


export const ProcessProvider: React.FC<{children: ReactChild}> = ({children}) => {
    const [process, setProcess] = useState<ProcessType[]>([])

    const [subProcess, setSubProcess] = useState<SubProcessType[]>([])

    const [selectedProcess, setSelectedProcess] = useState(1)

    const router = useRouter();
    const prodId = router.query.name || 1;

    React.useEffect(() => {
        if(router.pathname.includes('/details')){
            getParentProcess(prodId as string | number)
            .then(resPro => {
                const state = resPro?.data?.data?.attributes?.processLevels?.data?.map((pcs: any) => ({id: pcs.id, title: pcs.attributes.name}))
                setProcess(state);
                let storedProcess: any = localStorage.getItem('navotia_process');

                if(storedProcess || storedProcess?.process){
                    storedProcess = JSON.parse(storedProcess as string)          
                    console.log({ddd: Boolean(storedProcess), storedProcess});
                    
                    if(storedProcess?.process && storedProcess.process.some((sp: any) => sp.parent === selectedProcess)){
                        storedProcess = storedProcess.process.map((sp: any) => {
                            if(sp.productId === prodId){
                                sp.process = state;
                                return sp;
                            }
                            return sp;
                        })
                    }else{
                        storedProcess = {
                            process: [{
                                productId: prodId,
                                state
                            }]
                        } 
                    }

                    localStorage.setItem('navotia_process', JSON.stringify(storedProcess))

                }else{
                    localStorage.setItem('navotia_process', JSON.stringify({process: [
                        {
                            productId: prodId,
                            process: state 
                        }
                    ]}))
                }
            })
            .catch(err => console.log(err))
        }
    }, [prodId])

    React.useEffect(() => {
        if(router.pathname.includes('/details')){
            getSubProcess(selectedProcess as string | number)
            .then(resPro => {
                
                const state = resPro?.data?.data?.attributes?.subProcesses?.data?.map((pcs: any) => ({id: pcs.id, title: pcs.attributes.name, content: pcs.attributes.description, pid: selectedProcess, isComplete: false,}));
                console.log({state});
               
                setSubProcess(state)

                let storedSubProcess: any = localStorage.getItem('navotia_sub_process')
                    
                if(selectedProcess !== null) {
                    storedSubProcess = JSON.parse(storedSubProcess as string)
                    console.log({storedSubProcess});

                    if(storedSubProcess?.subProcess && storedSubProcess.subProcess.some((sp: any) => sp.parent === selectedProcess)){
                        storedSubProcess = storedSubProcess.subProcess.map((sp: any) => {
                            if(sp.parent === selectedProcess){
                                sp.state = state;
                                return sp;
                            }
                            return sp;
                        })
                    }else{

                        storedSubProcess = {
                            subProcess: [{
                                parent: selectedProcess,
                                state
                            }]
                        } 

                    }
                     
                    localStorage.setItem('navotia_sub_process', JSON.stringify(storedSubProcess))
                }else{
                    localStorage.setItem('navotia_sub_process', JSON.stringify({subProcess: [
                        {
                            parent: selectedProcess,
                            state
                        }
                    ]}))
                }

            })
            .catch(err => console.log(err))
        }
    }, [selectedProcess])

    const updateSelection = (id: number) => {
            setSelectedProcess(id)
    }
    
    const toggleSubProcess = (spId: number) => {
        const state = subProcess.map((pcs: SubProcessType) => {
            if(pcs.id === spId){
                pcs.isComplete = !pcs.isComplete
                return pcs;
            }
            
            return pcs;
        });

        setSubProcess(state);

        let storedSubProcess: any = localStorage.getItem('navotia_sub_process')

        if(!storedSubProcess) return;

        storedSubProcess = JSON.parse(storedSubProcess as string)

        storedSubProcess.subProcess.map((sp: any) => {
            if(sp.parent ===  selectedProcess){
                sp.state = state
                return sp
            }

            return sp
        })
         
        localStorage.setItem('navotia_sub_process', JSON.stringify(storedSubProcess))        
    }

    return <ProcessContext.Provider value={{process, subProcess, updateSelection, selectedProcess, toggleSubProcess}}>
        {children}
    </ProcessContext.Provider>
}
