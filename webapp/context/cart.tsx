import React, { ReactChild, useState } from "react";

interface CartTypes {
    services: any[],
    addToCart: (id: string) => void,
    removeFromCart: (id: string) => void,
    emptyCart: () => void,
    ttlCost: number
}

export const CartContext = React.createContext<CartTypes>({
    services: [],
    addToCart: (id: string) => {},
    removeFromCart: (id: string) => {},
    emptyCart: () => {},
    ttlCost: 0
})

export const CartProvider: React.FC<{children: ReactChild}>  = ({children}) => {
    const [services, setServices] = useState<any[]>([]);
    const [ttlCost, setTtlcost] = useState<number>(0);

    React.useEffect(() => {
        if(localStorage && localStorage.getItem('navotia_cart')){
            const state = JSON.parse(localStorage.getItem('navotia_cart') as string)
            setServices(state.cart)
        }
    }, [])

    React.useEffect(() => {
        setTtlcost(services.length > 0 ? services.reduce((prevVal, curVal,) => prevVal + curVal.attributes.price , 0) : 0);
        
    }, [services])

    const addToCart = (product: any) => {
        const state = [...services, product]
        setServices(state)
        localStorage.setItem('navotia_cart', JSON.stringify({cart: state}))
    }

    const removeFromCart = (id: string) => {
        const state = services.filter((service: any) => `${service.id}` !== id)
        setServices(state);
        localStorage.setItem('navotia_cart', JSON.stringify({cart: state}))
    }

    const emptyCart = () => {
        setServices([])
        localStorage.setItem('navotia_cart', JSON.stringify({cart: []}))
    }

    return <CartContext.Provider value={{services, addToCart, removeFromCart, emptyCart, ttlCost}} >
        {children}
    </CartContext.Provider>
}