import React, { ReactChild, useState } from "react";
import { useRouter } from "next/router";

// SERVICES
import {getProducts, getTypesBySuper, getCategories, getProduct} from "services/api/store"

export type Levels = {
    st: number, t: number, cat: number, prod: number
}

export type LevelsEnum = 'st' | 't' | 'cat' | 'prod'

interface BreadcrumbsContextTypes {
    levels: Levels,
    clearAllLevels: VoidFunction, 
    clearLevel: (levelType: LevelsEnum) => void, 
    addLevel: (levelType: LevelsEnum, id: number) => void 
    generateBreadCrumbs: () => Promise<string[]>
    handleClick: (lvl: LevelsEnum) => void
}

const defaultLevel = {st: 1, t: 1, cat: 1, prod: 2}

export const BreadcrumbsContext = React.createContext<BreadcrumbsContextTypes>({
    levels: defaultLevel,
    clearAllLevels: () => {}, 
    clearLevel: (levelType: LevelsEnum) => {}, 
    addLevel: (levelType: LevelsEnum, id: number) => {},
    generateBreadCrumbs: async () => [],
    handleClick: (lvl: LevelsEnum) => {}
})

export const BreadcrumbsProvider: React.FC<{children: ReactChild}> = ({children}) => {

    const [levels, setLevels] = useState<Levels>(defaultLevel);

    const router = useRouter()

    React.useEffect(() => {
        if(sessionStorage && sessionStorage.getItem('novatio_breadcrumbs')) {
            setLevels(JSON.parse(sessionStorage.getItem('novatio_breadcrumbs') as string)?.levels || defaultLevel)   
        }
    }, [])

    const clearAllLevels = () => {
        setLevels(defaultLevel)
        sessionStorage.setItem('novatio_breadcrumbs', JSON.stringify({levels: defaultLevel}))
    }

    const clearLevel = (levelType: LevelsEnum) => {
        const state = {...levels, [levelType]: 1}
        setLevels(state)
        sessionStorage.setItem('novatio_breadcrumbs', JSON.stringify({levels: state}))
    }

    const addLevel = (levelType: LevelsEnum, id: number) => {
        const state = {...levels, [levelType]: id}
        setLevels(state)
        sessionStorage.setItem('novatio_breadcrumbs', JSON.stringify({levels: state}))
    }

    const generateBreadCrumbs = async (): Promise<string[]> => {
       try{
            const superType = await getTypesBySuper(levels.st)
            const type = await getCategories(levels.t)
            const category = await getProducts(levels.cat)
            
            if(router.pathname.includes('/details')){
                const product = await getProduct(levels.prod)
            
                return [superType, type, category, product].map(({data}: any) => data.data.attributes.name)
            }

            return [superType, type, category].map(({data}: any) => data.data.attributes.name)
       }catch(err: any){
           console.log(err);
           return [];
       }
    }

    const handleClick = (level: LevelsEnum) => {
        const path = {
            st: '/product-types?superType=', 
            t: '/services?catId=', 
            cat: '/services?catId=',
            prod: 'details/'
        }

        if(level === 'prod'){
            router.push(`${path[level]}${levels[level]}/?catId=${levels[level]}`)
            return
        }

        router.push(`${path[level]}${levels[level]}`)
    }
    

    return <BreadcrumbsContext.Provider value={{levels, clearAllLevels, clearLevel, addLevel, generateBreadCrumbs, handleClick}}>
        {children}
    </BreadcrumbsContext.Provider>
}
