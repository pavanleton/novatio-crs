const colors = {
    brand:
        {
            50: '#e2fceb',
            100: '#bef2cc',
            200: '#96e7ac',
            300: '#6ede8c',
            400: '#46d46c',
            500: '#2ebb52',
            600: '#21913f',
            700: '#16682c',
            800: '#083e19',
            900: '#001703',
        },
    heading: "#011627",
};

export const ColorPallete = { colors };