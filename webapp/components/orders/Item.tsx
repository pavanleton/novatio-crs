import { Flex, Heading, Text, IconButton } from "@chakra-ui/react";
import {FiTrash2} from 'react-icons/fi'
import { useContext } from "react";
import dayjs from "dayjs";

// CONTEXT
import { CartContext } from "context/cart";

const OrderItem: React.FC<any> = (props) => {


  return (
    <>
      <Flex gap={'1rem'} border={'1px solid white'} w={'100%'} justify={'space-between'} alignItems={'center'} p={'10px'} borderRadius={'8px'}>
        <Flex flexDir={'column'} gap={'10px'} w={'85%'} >
          <Heading size={'sm'} as={'h5'} noOfLines={1}>{props?.data?.attributes?.productName|| ''}</Heading>

          <Text noOfLines={2} isTruncated fontSize={'xs'} w={'100%'} fontWeight={500}>
            {dayjs(props?.data?.attributes?.createdAt).format('DD MMM YY hh:mm') || ''}
          </Text>
          
          <Text>
            {`$${props?.data?.attributes?.price || 0}`}
          </Text>
        </Flex>

      </Flex>
    </>
  );
};

export default OrderItem;
