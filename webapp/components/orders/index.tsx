import { IconButton, Box, Badge, useDisclosure } from "@chakra-ui/react";
import { FiShoppingCart } from "react-icons/fi";
import { SiHackthebox } from "react-icons/si";

import { useContext } from "react";

// COMPONENTS
import OrderModal from "./modal";
import Checkout from "components/Checkout";

// CONTEXT
import { CartContext } from "context/cart";

const Order: React.FC = () => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const { isOpen: isCheckOutOpen, onOpen: openCheckout, onClose: closeCheckout } = useDisclosure()

    return <>
        <Checkout isOpen={isCheckOutOpen} onClose={closeCheckout} />
        <OrderModal {...{isOpen, onClose}} openCheckout={openCheckout} />
        <Box pos={'relative'}>
            <IconButton icon={<SiHackthebox/>} aria-label={'Order icon'} variant={'ghost'} colorScheme={'dark'} onClick={onOpen} />
        </Box>
    </>
}

export default Order;