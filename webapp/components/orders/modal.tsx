import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Flex,
  Text
} from "@chakra-ui/react";
import React, { useContext, useState } from 'react'

// COMPONENT
import CartItem from "./Item";

// CONTEXT
import { CartContext } from "context/cart";

// SERVICES
import { getUser } from "services/getUser";

const OrderModal: React.FC<{isOpen: boolean, onClose: VoidFunction, openCheckout: VoidFunction}> = ({isOpen, onClose, openCheckout}) => {
    const [orders, setOrders] = useState([]);
    const {services: cart, ttlCost} = useContext(CartContext);

    React.useEffect(() => {
      let ordersInStore: any = localStorage.getItem('navotia_orders')

      if(!ordersInStore) {
        setOrders([])
        return;
      }

      if(ordersInStore){
        ordersInStore = JSON.parse(ordersInStore);
        const userId = getUser().user.id
        const state = ordersInStore.filter((order: any) => order.user === userId)
        setOrders(state.length > 0 ? state[0].orders : [])
      }

    }, [])

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={'lg'}>
        <ModalOverlay />
        <ModalContent 
            bg={'black'} color={'white'}
        >
          <ModalHeader>Your Orders</ModalHeader>
          <ModalCloseButton />
          <ModalBody >
            <Flex flexDir={'column'} gap={'10px'}>
                {
                   orders.length > 0 
                    ? orders.map((order: any, index: number) => <CartItem key={index} {...order} />)
                    : <Text textAlign={'center'}>No recent orders were placed!</Text>
                }
            </Flex>
          </ModalBody>
                 
          <ModalFooter justifyContent={'space-between'}>           
            <Button variant="ghost"  mr={3} onClick={onClose}>
                 Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default OrderModal;
