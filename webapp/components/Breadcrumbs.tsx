import { Flex, IconButton, Text } from "@chakra-ui/react";
import {VscTriangleRight} from 'react-icons/vsc';
import React, { useContext, useState } from "react";

// COMPONENTS
import Cart from "./cart";
import Order from "./orders";

// CONTEXT
import { BreadcrumbsContext } from "context/breadCrumbs";

const Item: React.FC<{name: string, isLast?: boolean, id: number, handleClick: any}> = ({name, isLast, id, handleClick}) => {
  const level = {
    1: 'st',
    2: 't',
    3: 'cat',
    4: 'prod'
  }
  return <>
            <Flex
            gap={'5px'}
            alignItems={"center"}
            cursor={'pointer'}
            // @ts-ignore
            onClick={() => handleClick(level[id + 1])}
          >
              <Text>{name}</Text>
              {!isLast && <VscTriangleRight size={'10px'} />}
          </Flex>
  </>
}

const Breadcrumbs: React.FC = () => {
  const [path, setPath] = useState([])

  const {handleClick, generateBreadCrumbs} = useContext(BreadcrumbsContext)

  React.useEffect(() => {
    generateBreadCrumbs().then((data: any)=> {
      setPath(data);
    })
  }, [generateBreadCrumbs])
  

  return (
    <>
      <Flex
        w={"100%"}
        gridGap={"2rem"}
        alignItems={"center"}
        justifyContent={"space-evenly"}
      >
        <Flex
          w={"80%"}
          bg={"#121212"}
          px={"2rem"}
          py={"15px"}
          borderRadius={"10px"}
          gap={'5px'}
          alignItems={"center"}
        >

          {
            path.map((level: string, index: number) => <Item name={level} key={index} isLast={index === (path.length - 1)} id={index} handleClick={handleClick} />)
          }

        </Flex>
          <Flex>
            <Order />
            <Cart />
          </Flex>
      </Flex>
    </>
  );
};

export default Breadcrumbs;
