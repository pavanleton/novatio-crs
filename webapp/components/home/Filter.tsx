import {
  Flex,
  Text,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuItemOption,
  MenuGroup,
  MenuOptionGroup,
  MenuDivider,
  Button,
} from "@chakra-ui/react";
import React from "react";
import { FiFilter } from "react-icons/fi";
import { VscTriangleDown } from "react-icons/vsc";
import { AiOutlineLine } from 'react-icons/ai';


const Filter: React.FC = () => {

  const industries = [
    "Manufacturing",
    "Transportain & Storage",
    "Retail & Wholesale",
    "Education",
    "Government",
    "Healthcare",
    "Information & Communication",
    "BFSI",
    "Oil, Gas & Energy",
    "Real Estate"
  ]  

  return (
    <>
      <Flex bg={'#121212'} px={'2rem'} py={'15px'} borderRadius={'10px'} gridGap={'10px'} alignItems={'center'} justify={'space-between'} >
        <Flex alignItems={'center'} gridGap={'10px'} color={'#777777'}>
            <FiFilter />
            <Menu>
            <MenuButton as={Button} bg={'transparent'} alignItems={'center'} variant={'unstyled'} _focus={{outline: 'none'}} >
                Filter by Industries
            </MenuButton>
            <MenuList bg={'#000'} borderColor={'#e5e5e5'} borderWidth={'.5px'}>
                {
                    industries.map((industry: string, index: number) => 
                        <MenuItem key={index} _hover={{bg: '#121212'}}>{industry}</MenuItem>)
                }
            </MenuList>
            </Menu>
        </Flex>
        <Flex alignItems={'center'} color={'#777777'}>
            <AiOutlineLine style={{transform: 'rotate(90deg)'}} />
            <VscTriangleDown />
        </Flex>
        
      </Flex>
    </>
  );
};

export default Filter;
