import { Flex, Grid, Box } from "@chakra-ui/react";
import React, { useContext } from "react";

// COMPONENTS
import ProcessItem from "./process/Item";
import TimeLine from "./process/Timeline";

// CONTEXT
import { ProcessContext, ProcessType, SubProcessType } from "context/process";

const ProcessLevel: React.FC = () => {

  const {selectedProcess, process, subProcess, updateSelection, toggleSubProcess} = useContext(ProcessContext);

  const handleProcessSelect = (event: any) => {
    updateSelection(parseInt(event.target.id))
  }

  return (
    <>
      <Grid
        gridTemplateColumns={{ base: "1fr", md: "1fr 2fr" }}
        gridGap={"1rem"}
      >
        <Flex
          flexDirection={{ base: "row", md: "column" }}
          gridGap={"1rem"}
          overflowX={"scroll"}
          
          css={{
            '&::-webkit-scrollbar':{
              display: 'none'
            }
          }}
        >
            {
                process.map((pcs: ProcessType) => 
                    <ProcessItem {...pcs} key={pcs.id} isSelected={selectedProcess === pcs.id} subProcess={subProcess.filter((subpcs: SubProcessType) => subpcs.pid === pcs.id)} handleClick={handleProcessSelect} />
                )
            }

        </Flex>
        <Box px={"1.5rem"} py={"2rem"} bg={"#121212"} borderRadius={"10px"}>
          <TimeLine />
        </Box>
      </Grid>
    </>
  );
};

export default ProcessLevel;
