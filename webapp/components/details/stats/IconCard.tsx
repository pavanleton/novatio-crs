import { Flex, Heading, Text, Box, Input } from "@chakra-ui/react";
import React from "react";

const IconCard: React.FC<{ heading: string; value: string, subValue?: string, Icon: any, editable?: boolean, handlechange: any }> = ({
  heading,
  value,
  subValue,
  Icon,
  editable,
  handlechange
}) => {
  return (
    <>
      <Flex h={'100%'} bg={"#121212"} p={"1.5rem"} flexDir={'column'} alignItems={'flex-start'} borderRadius={'10px'}>
        <Box w={'80px'} h={'80px'} mb={'10px'}>
            <Icon />
        </Box>
        <Heading as={"h6"} size={"sm"} mt={'10px'} color={"brand.600"}>
          {heading}
        </Heading>
        <Flex>
          <Input value={value} readOnly={!editable} contentEditable={editable} variant={'unstyled'} fontSize={'3xl'} fontWeight={'bold'} color={'white'} onChange={(e: any) => handlechange(e.target.value)} />
          <Text contentEditable={false} fontSize={'xs'}>{subValue}</Text>
        </Flex>
      </Flex>
    </>
  );
};

export default IconCard;
