import { Flex, Text, Checkbox } from "@chakra-ui/react";
import React from "react";

const Item: React.FC<{ title: string; id: number; isChecked?: boolean; isSelected?:boolean, subProcess: any[], handleClick: (e: any) => void }> = ({
  title,
  isChecked,
  isSelected,
  subProcess,
  handleClick,
  id
}) => {

  const isCompleted = subProcess.length > 0 ? subProcess.every((process: {isComplete: boolean}) => Boolean(process.isComplete)) : false
  
  return (
    <>
      <Flex
        border={"2px solid"}
        borderColor={isSelected ? "brand.600" : '#121212'}
        p={"1rem"}
        gridGap={"10px"}
        borderRadius={"10px"}
        w={"100%"}
        bg={'#121212'}
        id={`${id}`}
        onClick={handleClick}
        cursor={'pointer'}
      >
        <Checkbox
          colorScheme={"brand"}
          isChecked={isCompleted}
          disabled
          borderColor={"brand.600"}
        />
          
        <Text color={"brand.600"} fontWeight={700} id={title}
        onClick={handleClick}>{title}</Text>  
      </Flex>
    </>
  );
};

export default Item;
