import {
  Box,
  Text,
  Flex,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from "@chakra-ui/react";
import { useContext } from "react";

// CONTEXT
import { ProcessContext, ProcessType, SubProcessType } from "context/process";


const Item: React.FC<{title: string, isChecked?: boolean, id: number}> = ({title, children, isChecked, id}) => {
  
  const {toggleSubProcess} = useContext(ProcessContext)

  const checkProcess = () => {
    toggleSubProcess(id)
  }
  
  return (
    <>
      <Box
        padding={"10px 40px"}
        pos={"relative"}
        bg={"inherit"}
        w={"100%"}
        _after={{
          content: '""',
          pos: "absolute",
          w: "17px",
          h: "17px",
          left: 0,
          bg: isChecked ? "brand.600" : "#000",
          border: "2px solid",
          borderColor: "brand.600",
          top: "15px",
          borderRadius: "50%",
          zIndex: 1,
        }}
        onClick={checkProcess}
      >
        <Flex
          padding={"10px 15px"}
          bg={"#282828"}
          pos={"relative"}
          borderRadius={"6px"}
        >
          <Accordion defaultIndex={[0]} allowMultiple w={"100%"}>
            <AccordionItem w={"100%"} border={"none"}>
              <AccordionButton w={"100%"} 
              fontWeight={700}
              _focus={{
                  outline: 'none'
              }}
              _expanded={{
                  color: 'brand.600'  
              }}
              >
                {title}
              </AccordionButton>
              <AccordionPanel pb={4}>
                <Text fontSize={"sm"} fontWeight={400}>
                  {children}
                </Text>
              </AccordionPanel>
            </AccordionItem>
          </Accordion>
        </Flex>
      </Box>
    </>
  );
};

export default Item;
