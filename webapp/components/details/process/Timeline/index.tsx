import { Flex, Box, Heading } from "@chakra-ui/react";
import { useContext } from "react";

// COMPONENTS
import TimelineItem from "./Item";

// CONTEXT
import { ProcessContext, ProcessType, SubProcessType } from "context/process";


const TimeLine: React.FC = () => {
  const {selectedProcess, process, subProcess, updateSelection, toggleSubProcess} = useContext(ProcessContext);

  return (
    <>
      <Flex flexDir={"column"} gridGap={'1rem'}>
        <Box>
          <Heading as={"h4"} size={"md"}>
            {process.filter((pcs: ProcessType) => pcs.id === selectedProcess)[0]?.title || ''}
          </Heading>
        </Box>
        <Box
          w={"100%"}
          boxSizing={"border-box"}
          position={"relative"}
          maxW={"1200px"}
          margin={"0 auto"}
          _after={{
            content: '""',
            pos: "absolute",
            width: "2px",
            bg: "brand.600",
            top: "20px",
            bottom: 0,
            left: "10px",
            ml: "-3px",
          }}
        >
          {
            subProcess.filter((sPcs: SubProcessType) => sPcs.pid === selectedProcess).map((process: SubProcessType, index: number) => 
              <TimelineItem {...process} isChecked={process.isComplete} key={index}>
              {process.content}
            </TimelineItem>
            )
          }
        </Box>
      </Flex>
    </>
  );
};

export default TimeLine;
