import { Box, Image, Text, Button, Flex, Heading } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useContext } from "react";

// CONTEXT
import { CartContext } from "context/cart";
import { BreadcrumbsContext } from "context/breadCrumbs";

const ServicesCard: React.FC<any> = (props) => {
  const { image, name, description, isInCart, id } = props;

  const router = useRouter();
  const { addToCart, removeFromCart } = useContext(CartContext);
  const { addLevel } = useContext(BreadcrumbsContext);

  const redirectToDetails = () => {
    addLevel('prod', parseInt(`${id}`));
    router.pathname = `/details/${id}`;
    router.replace(router);
  };

  return (
    <>
      <Box position={"relative"} bg={"#272727"} rounded={"lg"}>
        <Box h={"200px"} w={"100%"}>
          <Image
            objectFit={"cover"}
            h={"100%"}
            w={"100%"}
            src={image || 'https://i.ibb.co/qxPdhwz/Group-1.png'}
            alt={"Cover image"}
            rounded={"lg"}
            boxShadow={"lg"}
          />
        </Box>

        <Flex
          w={"100%"}
          gridGap={"10px"}
          flexDirection={"column"}
          bottom={"10px"}
          zIndex={20}
          bg={"#121212"}
          p={"1rem"}
        >
          <Heading size={"md"} as={"h5"}>
            {name}
          </Heading>
          <Text noOfLines={3}>{description}</Text>
          <Flex gap={"15px"}>
            <Button
              w={"75%"}
              bg={"#000"}
              _hover={{ bg: "#121212" }}
              onClick={redirectToDetails}
            >
              View
            </Button>
            <Button
              w={"75%"}
              colorScheme={"brand"}
              onClick={
                isInCart
                  ? () => removeFromCart(`${id}`)
                  : () => addToCart(props)
              }
            >
              {isInCart ? "Remove from cart" : "Add to cart"}
            </Button>
          </Flex>
        </Flex>
      </Box>
    </>
  );
};

export default ServicesCard;
