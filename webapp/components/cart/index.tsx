import { IconButton, Box, Badge, useDisclosure } from "@chakra-ui/react";
import { FiShoppingCart } from "react-icons/fi";
import { useContext } from "react";

// COMPONENTS
import CartModal from "./modal";
import Checkout from "components/Checkout";

// CONTEXT
import { CartContext } from "context/cart";

const Cart: React.FC = () => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const { isOpen: isCheckOutOpen, onOpen: openCheckout, onClose: closeCheckout } = useDisclosure()

    const {services: cart} = useContext(CartContext)

    return <>
        <Checkout isOpen={isCheckOutOpen} onClose={closeCheckout} />
        <CartModal {...{isOpen, onClose}} openCheckout={openCheckout} />
        <Box pos={'relative'}>
            <IconButton icon={<FiShoppingCart/>} aria-label={'cart icon'} variant={'ghost'} colorScheme={'dark'} onClick={onOpen} />
            {cart.length > 0 && <Badge pos={'absolute'} top={0}>{cart.length}</Badge>}
        </Box>
    </>
}

export default Cart;