import { Flex, Heading, Text, IconButton } from "@chakra-ui/react";
import {FiTrash2} from 'react-icons/fi'
import { useContext } from "react";

// CONTEXT
import { CartContext } from "context/cart";

const CartItem: React.FC<any> = (props) => {

  const {removeFromCart} = useContext(CartContext)

  return (
    <>
      <Flex gap={'1rem'} border={'1px solid white'} w={'100%'} justify={'space-between'} alignItems={'center'} p={'10px'} borderRadius={'8px'}>
        <Flex flexDir={'column'} gap={'10px'} w={'85%'} >
          <Heading size={'sm'} as={'h5'}>{props?.name || ''}</Heading>

          <Text noOfLines={2} isTruncated fontSize={'xs'} w={'100%'} fontWeight={500}>
            {props?.attributes?.summary || ''}
          </Text>
          
          <Text>
            {`$${props.attributes.price}`}
          </Text>
        </Flex>

        <IconButton size={'sm'} icon={<FiTrash2/>} aria-label={'Delete item from cart'} variant={'ghost'} colorScheme={'red'} onClick={() => removeFromCart(`${props?.id}`)} />
      </Flex>
    </>
  );
};

export default CartItem;
