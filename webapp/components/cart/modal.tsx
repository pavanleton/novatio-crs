import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Flex,
  Text
} from "@chakra-ui/react";
import React, { useContext } from 'react'

// COMPONENT
import CartItem from "./Item";

// CONTEXT
import { CartContext } from "context/cart";

const CartModal: React.FC<{isOpen: boolean, onClose: VoidFunction, openCheckout: VoidFunction}> = ({isOpen, onClose, openCheckout}) => {

    const {services: cart, ttlCost} = useContext(CartContext);

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={'lg'}>
        <ModalOverlay />
        <ModalContent 
            bg={'black'} color={'white'}
        >
          <ModalHeader>Cart</ModalHeader>
          <ModalCloseButton />
          <ModalBody >
            <Flex flexDir={'column'} gap={'10px'}>
                {
                   cart.length > 0 
                    ? cart.map((prod: any, index: number) => <CartItem key={index} {...prod} />)
                    : <Text textAlign={'center'}>Cart is empty!</Text>
                }
            </Flex>
          </ModalBody>
                 
          <ModalFooter justifyContent={'space-between'}>
          <Text float={'left'}>Total Cost: {`$${ttlCost}`}</Text> 
           
           <Flex alignItems={'center'}>
               <Button variant="ghost"  mr={3} onClick={onClose}>
                 Close
               </Button>
               <Button colorScheme={'brand'} onClick={openCheckout}>Checkout</Button>
           </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CartModal;
