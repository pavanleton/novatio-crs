import {
  Box,
  Flex,
  Image,
  Text,
  IconButton,
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
} from "@chakra-ui/react";
import { BiCircle } from "react-icons/bi";
import { FiMenu } from "react-icons/fi";

const NavItems = () => {
  const industries = [
    "Manufacturing",
    "Transportain & Storage",
    "Retail & Wholesale",
    "Education",
    "Government",
    "Healthcare",
    "Information & Communication",
    "BFSI",
    "Oil, Gas & Energy",
    "Real Estate",
  ];

  return (
    <>
      <Flex flexDirection={"column"}>
        {industries.map((industry: string, index: number) => (
          <Flex
            key={index}
            bg={"#121212"}
            px={"1rem"}
            py={"15px"}
            borderRadius={"10px"}
            alignItems={"center"}
            justifyContent={"space-between"}
            fontSize={"lg"}
            cursor={"pointer"}
            _hover={{
              transition: "all .4s linear",
              bg: "#2D2D2D",
            }}
          >
            <Flex alignItems={"center"} gridGap={"10px"}>
              <BiCircle />
              <Text>{industry}</Text>
            </Flex>
          </Flex>
        ))}
      </Flex>
    </>
  );
};

const MainSideBar: React.FC = () => {
  const {
    isOpen: isSidebarOpen,
    onClose: closeSideBar,
    onOpen: openSideBar,
  } = useDisclosure();

  return (
    <>
      <Drawer isOpen={isSidebarOpen} placement="left" onClose={closeSideBar}>
        <DrawerOverlay />
        <DrawerContent bg={"#121212"}>
          <DrawerCloseButton />
          <DrawerHeader>
            <Image
              src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
              alt={"Novatio logo"}
            />
          </DrawerHeader>

          <DrawerBody>
            <NavItems />
          </DrawerBody>
        </DrawerContent>
      </Drawer>

      <Flex
        as={"aside"}
        bg={"#121212"}
        p={"2rem"}
        flexDirection={"column"}
        gridGap={"2rem"}
        h={'100vh'}
        overflowY={'scroll'}
      >
        <Flex
          w={{ base: "150px", md: "200px" }}
          pl={{ base: "none", lg: "1rem" }}
          justifyContent={{ base: "space-between", lg: "flex-start" }}
        >
          <IconButton
            icon={<FiMenu />}
            aria-label={"Sidebar"}
            variant={"dark"}
            onClick={openSideBar}
            d={{base: 'block', lg: 'none'}}
          />
          <Image
            src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
            alt={"Novatio logo"}
          />
        </Flex>
        <Box d={{ base: "none", lg: "flex" }}>
          <NavItems />
        </Box>
      </Flex>
    </>
  );
};

export default MainSideBar;
