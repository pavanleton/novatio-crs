import {
  Box,
  Flex,
  Image,
  Text,
  IconButton,
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
} from "@chakra-ui/react";
import { BiCircle } from "react-icons/bi";
import { FiMenu } from "react-icons/fi";
import { useRouter } from "next/router";
import { useContext } from "react";

// CONTEXT
import { BreadcrumbsContext } from "context/breadCrumbs";

const industries = [
  "Manufacturing",
  "Transportain & Storage",
  "Retail & Wholesale",
  "Education",
  "Government",
  "Healthcare",
  "Information & Communication",
  "BFSI",
  "Oil, Gas & Energy",
  "Real Estate",
];

const NavItems: React.FC<{ navs: any[] }> = ({ navs }) => {

  const router = useRouter();

  const {addLevel} = useContext(BreadcrumbsContext)
  
  const fetchProducts = (id: string) => {
    addLevel('cat', parseInt(`${id}`))
    router.query.pdId = id as string;
    router.pathname = '/services';
    router.push(router)
  }

  return (
    <>
      <Flex flexDirection={"column"}>
        {navs.map((category: any, index: number) => (
          <Flex
            key={index}
            bg={"#121212"}
            px={"1rem"}
            py={"10px"}
            borderRadius={"10px"}
            alignItems={"center"}
            gridGap={'2rem'}
            fontSize={"md"}
            cursor={"pointer"}
            _hover={{
              transition: "all .4s linear",
              bg: "#2D2D2D",
            }}
            onClick={() => fetchProducts(category.id)}
          >
            <Box fontSize={"20px"} borderRadius={"8px"}>
              <BiCircle />
            </Box>
            <Text>{category?.attributes?.name || ""}</Text>
          </Flex>
        ))}
      </Flex>
    </>
  );
};

const SubMenuSideBar: React.FC<{ list: any[] }> = ({ list }) => {
  const {
    isOpen: isSidebarOpen,
    onClose: closeSideBar,
    onOpen: openSideBar,
  } = useDisclosure();

  const router = useRouter();

  return (
    <>
      <Drawer isOpen={isSidebarOpen} placement="left" onClose={closeSideBar}>
        <DrawerOverlay />
        <DrawerContent bg={"#121212"}>
          <DrawerCloseButton />
          <DrawerHeader>
            <Image
              src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
              alt={"Novatio logo"}
            />
          </DrawerHeader>

          <DrawerBody>
            <NavItems />
          </DrawerBody>
        </DrawerContent>
      </Drawer>

      <Flex
        d={{ base: "none", lg: "flex" }}
        bg={"#121212"}
        p={"1rem"}
        h={"100vh"}
        flexDirection={'column'}
      >
        <Flex w={"50px"} h={"50px"} mx={"1rem"}>
            <Image
              src={"https://i.ibb.co/JRv6Vrc/Novatio-Icon.png"}
              alt={"Novatio logo"}
              w={"100%"}
              h={"100%"}
              cursor={'pointer'}
              onClick={() => router.push('/home')}
            />
          </Flex>
        

        <Flex pt={"1rem"} px={"10px"}>
          <NavItems navs={list || []} />
        </Flex>
      </Flex>

      <Flex
        d={{ base: "flex", lg: "none" }}
        as={"aside"}
        bg={"#121212"}
        p={"2rem"}
        flexDirection={"column"}
        gridGap={"2rem"}
      >
        <Flex
          w={{ base: "150px", md: "200px" }}
          pl={{ base: "none", lg: "1rem" }}
          justifyContent={{ base: "space-between", lg: "flex-start" }}
        >
          <IconButton
            icon={<FiMenu />}
            aria-label={"Sidebar"}
            variant={"dark"}
            onClick={openSideBar}
            d={{ base: "block", lg: "none" }}
          />
          <Image
            src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
            alt={"Novatio logo"}
          />
        </Flex>
      </Flex>
    </>
  );
};

export default SubMenuSideBar;
