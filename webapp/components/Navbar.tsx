import { Flex, Box, Image } from "@chakra-ui/react";
import { useRouter } from "next/router";

// COMPONENTS
import Cart from "./cart";
import Order from "./orders";

const Navbar = () => {

  const router = useRouter();

  return (
    <>
      <Flex
        justify={"space-between"}
        bg={"#121212"}
        px={"2rem"}
        py={"15px"}
        borderRadius={"10px"}
      >
        <Box w={"100px"}>
          <Image
            src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
            alt={"Novatio logo"}
            onClick={() => router.push('/home')}
            cursor={'pointer'}
          />
        </Box>

        <Flex>
            <Order />
            <Cart />
          </Flex>
      </Flex>
    </>
  );
};

export default Navbar;
