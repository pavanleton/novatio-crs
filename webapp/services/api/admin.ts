import { getAPIInstance } from "services/api/config";

export const getOrders = () =>
    getAPIInstance({ }).get('/orders')