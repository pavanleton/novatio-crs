import { getAPIInstance } from "services/api/config";
import qs from 'qs';

export const getSuperTypes = () =>
    getAPIInstance({}).get('/super-types', {
        params: {
            populate: 'coverImage'
        }
    })

export const getTypesBySuper = (superType: string | number) =>
    getAPIInstance({}).get(`/super-types/${superType}?${qs.stringify({
        populate: {
            types: {
                populate: ['svgIcon']
            }
        }
    })}`)

export const getCategories = (types: string | number) => 
    getAPIInstance({}).get(`/types/${types}?${qs.stringify({
        populate: ['categories']
    })}`)

export const getProducts = (categoryId: string | number) => 
    getAPIInstance({}).get(`/categories/${categoryId}?${qs.stringify({
        populate: {
            products: {
                populate: ['coverImage']
            }
        }
    })}`)

export const getProduct = (productId: string | number) => 
getAPIInstance({}).get(`/products/${productId}?${qs.stringify({
    populate: ['processLevel']
})}`)  

export const getParentProcess = (productId: string | number) => 
    getAPIInstance({}).get(`/products/${productId}?${qs.stringify({
        populate: ['processLevels']
    })}`)

export const getSubProcess = (processId: string | number) => 
    getAPIInstance({}).get(`/process-levels/${processId}?${qs.stringify({
        populate: ['subProcesses']
    })}`)