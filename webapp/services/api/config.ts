import axios from "axios";
const getToken = (cms: boolean = true) => {

    if(cms)
        return process.env.NEXT_PUBLIC_CMS_PUBLIC_TOKEN;

    if (localStorage && localStorage.novatioUser)
        return localStorage.novatioUser.token
    
    return ''
}
export const getAPIInstance = ({ excludeHeaderToken }: { excludeHeaderToken?: boolean }, useCMSToken: boolean = true) => {
    const config: { baseURL: string | undefined, headers?: { Authorization: string } } = {
        baseURL: process.env.NEXT_PUBLIC_CMS_API,
        headers: {
            'Authorization': `Bearer ${getToken(useCMSToken)}`,
        },
    };
    if (excludeHeaderToken)
        delete config?.headers;
    return axios.create(config);
};
// 9999899998
export const register =
    ({ email, password, username }: { email: string; password: string; username: string; }) =>
        getAPIInstance({ excludeHeaderToken: true }).post('/auth/local/register', {
            email,
            password,
            username,
});

export const login =
    ({ identifier, password }: { identifier: string; password: string; }) =>
        getAPIInstance({ excludeHeaderToken: true }).post('/auth/local', {
            identifier,
            password,
});

