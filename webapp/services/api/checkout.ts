import { getAPIInstance } from "services/api/config";

export const createOrder = (shipping: any, products: any[], price: string) => {
  const data = {
    name: `${shipping.fname} ${shipping.lname}`,
    Address: shipping.address,
    Address2: shipping.address2,
    City: shipping.city,
    State: shipping.state,
    Country: shipping.country,
    zipCode: shipping.zip,
    productName: products.join(", "),
    price,
  };

  return getAPIInstance({excludeHeaderToken: true}).post("/orders", {data});
};
