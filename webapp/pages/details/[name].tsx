import { NextPage } from "next";
import { Tabs, TabList, TabPanels, Tab, TabPanel, Box } from "@chakra-ui/react";
import { useRouter } from "next/router";

// COMPONENTS
import ExecutiveLevel from "components/details/ExecutiveLevel";
import ProcessLevel from "components/details/ProcessLevel";

// LAYOUT
import ServicesLayout from "layout/services";

// TYPES
import PageWithLayoutType from "types/pageWithLayout";

const Details: NextPage = () => {

  const router= useRouter()
  const productId = router.query.name;

  return (
    <>
      <Box mt={"2rem"} mx={'3rem'}>
        <Tabs variant="unstyled">
          <TabList gridGap={'1rem'}>
            <Tab
              border={"2px solid #000"}
              bg={"#282828"}
              color={"white"}
              borderRadius={"8px"}
              _selected={{ borderColor: "#25A146" }}
            >
              Executive Level
            </Tab>
            <Tab
              border={"2px solid #000"}
              bg={"#282828"}
              color={"white"}
              borderRadius={"8px"}
              _selected={{ borderColor: "#25A146" }}
            >
              Process Level
            </Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <ExecutiveLevel />
            </TabPanel>
            <TabPanel>
              <ProcessLevel />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </>
  );
};

(Details as PageWithLayoutType).layout = ServicesLayout;

export default Details;
