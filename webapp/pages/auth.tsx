import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    Checkbox,
    Stack,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue, Image, AlertIcon, Alert,
} from '@chakra-ui/react';
import {useEffect, useState} from "react";
import {login, register} from "services/api/config";
import {useRouter} from "next/router";

const config = {
    'login': {
        mainText: 'Sign in to your account',
        redirectText: 'Don\'t have an account?',
        buttonText: 'Sign In'
    },
    'register': {
        mainText: 'Create an account',
        redirectText: 'Already have an account?',
        buttonText: 'Create'
    },
}

export default function SimpleCard() {

    const [authMode, setAuthMode] = useState<string>('login');
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [serverError, setServerError] = useState<string>('');
    const router = useRouter();

    useEffect(() => {
        setServerError('')
    }, [email, password, authMode])

    const toggleMode = () =>
        authMode === 'login' ? setAuthMode('register'): setAuthMode('login');

    const handleSubmit = async () => {
        try {
            let res;
            if (authMode === 'login')
                res = await login({
                    identifier: email,
                    password,
                });
            else
                res = await register({
                    email,
                    password,
                    username: email.split('@')[0]
                });

            localStorage.setItem('novatioUser', JSON.stringify({token: res.data.jwt, user: res.data.user}))    

            router.push('/home');
        } catch (e) {
            // @ts-ignore
            setServerError(e.response?.data?.error?.message)
        }
    }
    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('black.50', 'white.800')}>

            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
                {/* eslint-disable-next-line @next/next/no-img-element */}
                <img
                    width={'100%'}
                    src={"https://i.ibb.co/DfkckKP/NOVATIO.png"}
                    alt={"Novatio logo"}
                />
                <Stack align={'center'}>
                    {/*// @ts-ignore*/}
                    <Heading fontSize={'4xl'}>{config[authMode]['mainText']}</Heading>
                    <Text fontSize={'lg'} color={'gray.600'}>
                        to continue to the marketplace
                    </Text>
                </Stack>
                <Box
                    rounded={'lg'}
                    bg={'#121212'}
                    boxShadow={'lg'}
                    p={8}>
                    <Stack spacing={4}>
                        <FormControl id="email">
                            <FormLabel>Email address</FormLabel>
                            <Input
                                type="email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                            />
                        </FormControl>
                        <FormControl id="password">
                            <FormLabel>Password</FormLabel>
                            <Input
                                type="password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                            />
                        </FormControl>
                        <Stack spacing={10}>
                            <Stack
                                direction={{ base: 'column', sm: 'row' }}
                                align={'start'}
                                justify={'space-between'}>
                                <Link
                                    color={'blue.400'}
                                    onClick={toggleMode}
                                >
                                    {/*// @ts-ignore*/}
                                    {config[authMode]['redirectText']}
                                </Link>
                                {/*<Link color={'blue.400'}>Forgot password?</Link>*/}
                            </Stack>
                            { serverError && <Stack
                                spacing={3}>
                                <Alert
                                    colorScheme={'brand'}
                                    bg={'black'}
                                    status='error'>
                                    <AlertIcon/>
                                    {serverError}
                                </Alert>
                            </Stack>
                                }
                            <Button
                                colorScheme={'brand'}
                                onClick={handleSubmit}
                                >
                                {/*// @ts-ignore*/}
                                {config[authMode]['buttonText']}
                            </Button>
                        </Stack>
                    </Stack>
                </Box>
            </Stack>
        </Flex>
    );
}