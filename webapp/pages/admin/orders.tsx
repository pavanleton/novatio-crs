import react, {useEffect, useState} from 'react';
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    Stack,
    Heading,
    Container
} from '@chakra-ui/react'
import {getOrders} from "services/api/admin";


const Orders = () => {
    const [orders, setOrders] = useState([])
    useEffect(() => {
        getOrders().then(res => {
            setOrders(res.data?.data)
        })
    }, []);
    return <>
        <Heading style={{ margin: '100px' }} as='h1' size='2xl'>
            Admin Panel
        </Heading>
        <Container  maxW={'container.sm'} py={12} px={6}>
            <Heading as='h5' size='2xl'>
                Orders
            </Heading>
        <Table variant='striped' colorScheme='white'>
            <Thead>
                <Tr>
                    <Th>Name</Th>
                    <Th>Address</Th>
                    <Th>State</Th>
                    <Th>Country</Th>
                    <Th>Zip</Th>
                    <Th>Order Placed At</Th>
                    <Th>Price</Th>
                    <Th>Product</Th>
                </Tr>
            </Thead>
            <Tbody>
                {
                    orders.length && orders.map(order =>
                        <Tr
                            style={{
                                cursor: 'pointer'
                            }}
                        >
                            <Td>{order.attributes?.name}</Td>
                            <Td>{order.attributes?.Address + ' ' + (order?.Address2 || ' ')}</Td>
                            <Td>{order.attributes?.State}</Td>
                            <Td>{order.attributes?.Country}</Td>
                            <Td>{order.attributes?.zipCode}</Td>
                            <Td>{order.attributes?.createdAt}</Td>
                            <Td>{order.attributes?.price}</Td>
                            <Td>{order.attributes?.productName}</Td>
                        </Tr>
                    )
                }
            </Tbody>
        </Table>
        </Container>
    </>
}
export default Orders;