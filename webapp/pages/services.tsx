import { NextPage } from "next";
import { Grid } from "@chakra-ui/react";
import React, { useContext, useState } from "react";
import { useRouter } from "next/router";

// COMPONENTS
import ServicesCard from "components/services/Card";

// LAYOUT
import ServicesLayout from "layout/services";

// CONTEXT
import { CartContext } from "context/cart";

// TYPES
import PageWithLayoutType from "types/pageWithLayout";

// SERVICES
import {getProducts} from "services/api/store";


const Services: NextPage = () => {
  const [products, setProducts] = useState([])

  const { services: cart } = useContext(CartContext)
  const router = useRouter();
  const productsId = router.query?.pdId || 1;

  React.useEffect(() => {
    getProducts(productsId as string | number)
      .then(resp => setProducts(resp?.data?.data?.attributes?.products?.data || []))
      .catch(err => console.log(err))
  }, [productsId])

  return (
    <>
      <Grid mt={"2rem"} mx={{base: '2rem', md: '3rem'}} gridTemplateColumns={{base: 'repeat(2, 1fr)', md: 'repeat(2, 1fr)', lg: 'repeat(3, 1fr) '}} gridGap={'1rem'}>
          {
            products.map((service: any, index: number) => 
                <ServicesCard 
                  key={index}
                  image={service?.attributes?.coverImage?.data?.attributes?.url|| ''}
                  name={service?.attributes?.name || ''}
                  description={service?.attributes?.summary || ''}
                  isInCart={cart.some(prod => `${prod.id}` === `${service?.id}`)} 
                  id={service.id}
                  {...service}
              />
            )
          }
      </Grid>
    </>
  );
};

(Services as PageWithLayoutType).layout = ServicesLayout;

export default Services;
