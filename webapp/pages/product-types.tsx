import { NextPage } from "next";
import { Container, Box, Flex, Image, Heading, Input, Grid, Button, Select, Text } from "@chakra-ui/react";
import {useContext, useEffect, useState} from "react";
import { useRouter } from "next/router";

// COMPONENTS
import Navbar from "components/Navbar";

// CONTEXT
import { BreadcrumbsContext } from "context/breadCrumbs";

// SERVICES
import {getTypesBySuper} from "services/api/store";

const IndustryCategories: React.FC<{name: string, icon: string, isSelected?: boolean, id: string}> = ({name, icon, isSelected, id}) => {
    
    const {addLevel} = useContext(BreadcrumbsContext)

    const router = useRouter();

    const redirectToServices = () => {
        addLevel('t', parseInt(`${id}`));
        router.push(`/services?catId=${id}`)
    }
    
    return <>
        <Flex 
            color={'white'} 
            flexDirection={'column'} alignItems={'center'} 
            gap={'10px'} cursor={'pointer'}
            w={'30px'}
            h={{base: '80px', lg: '100px'}}
            onClick={redirectToServices}
            _hover={{
                color: "#00e074",
                "& > img": {
                    filter: 'invert(29%) sepia(100%) saturate(380%) hue-rotate(84deg) brightness(89%) contrast(93%)'
                }
            }}
        >
            <Image src={icon} w={'100%'} />
            <Text fontSize={{base: 'xs', lg: 'xs'}} noOfLines={2}>{name}</Text>
        </Flex>
    </>
}

const ProductTypes: NextPage = () => {

    const router = useRouter();
    const productSuperType = router.query?.superType || 1;
    const [productTypes, setProductTypes] = useState([]);

    useEffect(() => {
        getTypesBySuper(productSuperType as string | number).then(res => {
            setProductTypes(res?.data?.data?.attributes?.types?.data || []);
        })
    }, []);


    return (
    <>
      <Box
        h={"100vh"}
        w={"100%"}
        background={
          "linear-gradient(180deg, rgba(5, 85, 27, 0.6) 3.44%, rgba(18, 18, 18, 0.2) 100%)"
        }
       
        pos={'relative'}
      >

        <Image
         src={'https://i.ibb.co/j3tX0N3/Intersect-min.png'}
         alt={'Bg image'} position={'absolute'} w={'100%'}
         h={'100%'} zIndex={-10}
         objectFit={'cover'}
        />  
        <Container  py={'2rem'} maxW={"container.xl"}>
          <Navbar />

          <Flex 
            flexDirection={'column'} 
            w={'100%'} h={'50vh'} 
            justify={'center'} alignItems={'center'}
            gridGap={'1rem'}
          >
            <Heading textAlign={'center'}>Discover the best automation practices</Heading>
            <Grid 
                w={'100%'} gridTemplateColumns={{base: '1fr', md: '1fr 1fr 1fr .3fr'}}
                bg={'#000'} alignItems={'center'}
                padding={'10px 1rem'}
                gridGap={{base: '1rem', md: '14px'}}
                rounded={{base: 'lg', md: 'full'}}
             >
                <Box w={'100%'}  borderRight={{lg: '1px solid #777777'}}>
                    <Input placeholder={'What are you looking for ?'} type={'search'} _placeholder={{color: '#777777'}} variant={'unstyled'} />
                </Box>
                <Box w={'100%'}  borderRight={{lg: '1px solid #777777'}}>
                    <Input placeholder={'Location'} type={'search'} _placeholder={{color: '#777777'}} variant={'unstyled'} />
                </Box>
                <Box w={'100%'}  borderRight={{lg: '1px solid #777777'}}>
                    <Select placeholder='All Catagories' variant={'unstyled'} color={'#777777'}>
                        <option value='option1'>Option 1</option>
                        <option value='option2'>Option 2</option>
                        <option value='option3'>Option 3</option>
                    </Select>                
                </Box>
                <Box w={'100%'}>
                    <Button w={'100%'} rounded={'full'} colorScheme={'brand'}>Search</Button>
                </Box>
            </Grid>

          </Flex>
        </Container>
        <Box pos={'absolute'} bottom={0} bg={'#000'} w={'100%'} h={{base: '280px', md: '300px', lg: '280px'}} p="1rem" overflowY={'scroll'} zIndex={10}  css={{
            '&::-webkit-scrollbar':{
              display: 'none'
            }
          }}>
            <Container maxW={'container.xl'} d={'flex'} flexDir={'column'} justifyContent={{base: 'flex-start', md: 'center'}} h={'100%'} py={'1rem'}>
                <Grid gridTemplateColumns={{base: 'repeat(4, 1fr)', md: 'repeat(6, 1fr)', lg: 'repeat(10, 1fr)'}} alignItems={'center'} gridGap={{base: '2rem', lg: '15px'}}>
                    {
                        productTypes.length && productTypes.map((t: any, i: number) =>
                            <IndustryCategories key={i} name={t.attributes.name} id={t.id} icon={t.attributes.svgIcon.data.attributes.url} />
                        )
                    }
                </Grid>    
            </Container>
        </Box>
      </Box>
    </>
  );
};

export default ProductTypes;
