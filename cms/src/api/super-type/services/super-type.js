'use strict';

/**
 * super-type service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::super-type.super-type');
